#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

Guindilla.new do

  canvas do
    draw('fill') do
      fill_color 'orange'
      arc(80, 80, 75, 180, 0)
      line_to(80, 150)
      line_to(5, 80)

    end

    draw('fill') do
      line_color 'green'
      fill_color 'yellow'
      move_to(45, 70)
      canvas_circle(35, 70, 10)
      move_to(115, 70)
      canvas_circle(125, 70, 10)
    end

  end

end
