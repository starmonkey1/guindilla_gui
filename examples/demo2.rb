#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

Guindilla.new(verbose: true) do

  text_align('center')
  font_family('sans')

  banner = heading("GuindillaGUI",
    background: 'crimson',
    color: 'white',
    border_radius: '10px'
  )

  color_input("pick a new color: ") do |col|
    banner.background = col
  end

  h_rule

  para("check out some shapes:")
  h_box do
    height('100px')
    margin('10px')
    justify('space-around')
    @cir = circle(50, color: 'red', hidden: true)
    @squ = square(100, color: 'green')
    @tri = triangle(116, color: 'blue', hidden: true)
  end

  v_box() do
    checkbox("circle ") do
      @cir.toggle
    end
    checkbox("square ", checked: true) do
      @squ.toggle
    end
    checkbox("triangle ") do
      @tri.toggle
    end
  end

  h_rule
  line_break

  v_box do
    t_i = text_input("tell me something: ") do |inpt|
      @mirror.text = "oh, do you mean... " + inpt.reverse + "?"
    end
    @mirror = para("oh, do you mean...?")

    radio_button("yes, that's what I mean. ") do
      @response.text = "I hear ya."
    end
    radio_button("no, that's not what I mean") do
      @response.text = "I think you've got it backwards."
    end
    @response = para("well?")
  end

  h_rule
  line_break

  h_box(justify: 'center') do
    song = audio_out
    song.volume = 0.5

    file_input("got an audio file around? ", margin: '10px') do |file|
      song.source = file
    end

    pp_btn = button("play it", width: '40%', margin: '10px') do
      if song.state == "playing"
        song.pause
        pp_btn.text = "play it"
      else
        song.play
        pp_btn.text = "pause it"
      end
    end

    range_slider("volume", min: 0.0, max: 1.0, value: 0.5, step: 0.05) do |vol|
      song.volume = vol
    end

  end

  h_rule
  line_break

  url_input("enter a URL and try the nifty web frame: ") do |url|
    wf.source = url
  end
  line_break
  wf = web_frame

  h_rule

end #Guindilla
