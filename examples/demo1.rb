#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

images = []
(2..6).each do |n|
  images << "http://poignant.guide/images/chapter.poignant.guide-#{n}.jpg"
end

def rand_color
  "rgb(#{rand(200)}, #{rand(200)}, #{rand(200)})"
end


Guindilla.new do
  font_family('sans')
  text_align('center')
  align('center')

  banner = heading("GuindillaGUI",
    width: '90%',
    border_radius: '10px',
    background: 'crimson',
    color: 'white'
  )
  banner.transition('background', 1, "ease-out")

  pic = image('http://poignant.guide/images/chapter.poignant.guide-2.jpg')
  pic.transition("rotate", 1)

  button("go ahead, push me.", width: '40%', margin: '20px') do
    banner.background = rand_color
    pic.source = images[rand(5)]
    pic.rotate(360)
  end

end
