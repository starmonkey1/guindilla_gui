#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

Guindilla.new do
  v_box(width: '100%', height: '500px', border: 'solid 1px') do
    on_mouse_move do |x, y|
      circle(10).move_to(x - 5, y - 5)
    end
  end
end
