#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

x_array = (-6..6).to_a
square_array = []
cube_array = []
tan_array = []

x_array.each do |x|
  square_array << x ** 2
  cube_array << x ** 3
  tan_array << Math.tan(x) ** 3
end


Guindilla.new do
  chart(title: "graph") do
    plot(x: x_array, y: square_array, name: 'squares', mode: 'lines')
    plot(x: x_array, y: cube_array, name: 'cubes', mode: 'markers')
    plot(
      x: x_array,
      y: tan_array,
      name: 'cubed tangents',
      line: {color: 'pink', width: 1},
      marker: {color: 'lightblue', size: 8}
    )

    x_axis(title: 'x axis', showgrid: false, zeroline: false)
    y_axis(title: 'y axis', showgrid: false)

    legend(y: 0.5, font: {size: 16})
  end

  chart(type: "bar", title: "bar chart", showlegend: false) do
    plot(x: ["giraffes", "orangutans", "monkeys"], y: [20, 14, 23])
  end

  chart(type: "pie", title: "pie chart") do
    plot(values: [19, 26, 55], labels: ["foo", "bar", "baz"])
  end
end
