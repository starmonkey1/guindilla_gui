#require File.expand_path("../../lib/guindilla_gui.rb", __FILE__)
require 'guindilla_gui'
include GuindillaGUI

def to_balanced_ternary(n)
  @negative = false
  if n < 0
    @negative = true
    n = n.abs
  elsif n == 0
    return "0"
  end

  def to_ternary(n)
    digits = []
    while n > 0
      digits.prepend(n % 3)
      n = n / 3
    end
    return digits
  end

  def swap_and_carry(index, digit)
    @t_set[index] = digit
    if @t_set[index + 1]
      @t_set[index + 1] += 1
    else
      @t_set << 1
    end
  end

  def negate(bt_string)
    negs = {"1" => "T", "T" => "1"}
    @balanced_t = bt_string.gsub(Regexp.union(negs.keys), negs)
  end

  @t_set = to_ternary(n).reverse
  @t_set.each_with_index do |d, i|
    if d == 2
      swap_and_carry(i, "T")
    elsif d == 3
      swap_and_carry(i, 0)
    end
  end

  @balanced_t = @t_set.reverse.join("")
  negate(@balanced_t) if @negative
  return @balanced_t

end #to_balanced_ternary

def get_time
  t = Time.now
  h = t.strftime("%H")
  m = t.strftime("%M")
  s = t.strftime("%S")
  time_string = "#{h}:#{m}:#{s}"
end

def get_bt_time
  time_array = get_time.split(":")
  h = time_array[0].to_i
  m = time_array[1].to_i
  s = time_array[2].to_i

  if s > 30
    m += 1
    s = s - 60
  end

  if m > 30
    h +=1
    m = m - 60
  end

  pad_bt_time("#{to_balanced_ternary(h)}:#{to_balanced_ternary(m)}:#{to_balanced_ternary(s)}")
end

def pad_bt_time(bt_string)
  raw = bt_string.split(":")
  raw.each do |n|
    n.prepend("0") while n.length < 4
  end
  padded = raw.join(":")
end



gui = Guindilla.new do
  text_align("center")
  font_family("sans")
  heading("GuindillaGUI").background("crimson").color("white")
  heading(2, "Balanced Ternary Time")

  v_box do
    align("center")
    @bt_time = heading(3, get_bt_time).border("2px crimson solid").padding("25px")
    h_box do
      @time = para(get_time).border("1px solid green").padding("25px")
      @time.hide
    end
  end

  clock = every(1) do
    @bt_time.text = get_bt_time
  end

  @bt_time.on_hover do
    @time.text = get_time
    clock.toggle
    @time.toggle
  end

  @bt_time.on_leave do
    clock.toggle
    @time.toggle
  end

end
