
# GuindillaGUI


## Description
Libray for creating browser-based GUIs in Ruby

This library is still in a bare-bones **wicked-alpha** state, and is subject to fires, floods, and radical changes!

Tested on Linux (Arch 5.1x.x-lts) with Ruby 3.0.x

## Installation
Should be as easy as:
`gem install guindilla_gui`

## Usage
**Chrome and Chromium browsers won't work with GuindillaGui yet - sorry!**

Try setting your default browser to Firefox for now if you run into problems, better solutions are on the way!

Take a look at the **[wiki](https://gitlab.com/starmonkey1/guindilla_gui/-/wikis/home)** and the examples. Basic usage goes something like this:
```
require 'guindilla_gui'
include GuindillaGUI

# your regular old ruby logic goes here #

Guindilla.new do

    # your nifty GuindillaGUI methods go here #

end
```
Normally `Guindilla` will be the only class you need to explicitly instantiate.
Other classes are instantiated by methods called inside the `Guindilla` block, e.g.:
```
image('my_image.jpg')
button("push me") do
  ...
end
```
Here's a look at `demo1.rb` from the examples for a better idea of how this all works...
```
require 'guindilla_gui'
include GuindillaGUI

images = []
(2..6).each do |n|
  images << "http://poignant.guide/images/chapter.poignant.guide-#{n}.jpg"
end

def rand_color
  "rgb(#{rand(200)}, #{rand(200)}, #{rand(200)})"
end


Guindilla.new do
  font_family('sans')
  text_align('center')
  align('center')

  banner = heading("GuindillaGUI",
    width: '90%',
    border_radius: '10px',
    background: 'crimson',
    color: 'white'
  )
  banner.transition('background', 1, "ease-out")

  pic = image('http://poignant.guide/images/chapter.poignant.guide-2.jpg')
  pic.transition("rotate", 1)

  button("go ahead, push me.", width: '40%', margin: '20px') do
    banner.background = rand_color
    pic.source = images[rand(5)]
    pic.rotate(360)
  end

end
```

## Documentation
[https://rubydoc.info/gems/guindilla_gui/GuindillaGUI/Guindilla](https://rubydoc.info/gems/guindilla_gui/GuindillaGUI/Guindilla)

## Support
contact-project+starmonkey1-guindilla-gui-35487921-issue-@incoming.gitlab.com

No guarantees, but I'll do what I can!

## Contributing
Yes, Please!!

contact-project+starmonkey1-guindilla-gui-35487921-issue-@incoming.gitlab.com

## Acknowledgments
thanks to Matz, _why, and especially The Dude for being thoughtful, thought-provoking, and just generally awesome.<br>
thanks to ashbb and the Shoes gang for help way back when.<br>
thanks and praises to the most high.

## License
yeah, well since we've got to do this business -

GPL-3.0-or-later
