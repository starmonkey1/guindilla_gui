# frozen_string_literal: true

require_relative "lib/guindilla_gui/version"

Gem::Specification.new do |spec|
  spec.name = "guindilla_gui"
  spec.version = GuindillaGUI::VERSION
  spec.summary = "Ruby library for creating browser-based graphical user interfaces."
  #spec.description = "Ruby library for creating browser-based graphical user interfaces."
  spec.authors = ["lljk"]
  spec.license = "GPL-3.0-or-later"
  spec.email = ["j.kiddin@proton.me"]
  spec.homepage = "https://gitlab.com/starmonkey1/guindilla_gui"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/starmonkey1/guindilla_gui"
  spec.metadata["documentation_uri"] = "https://rubydoc.info/gems/guindilla_gui"

  spec.extra_rdoc_files = Dir["README.md", "CHANGELOG.md", "LICENSE.md"]
  spec.rdoc_options << '--title' << 'GuindillaGui' <<
    '--exclude' << 'resources' <<
    '--markup' << 'markdown' <<
    '--main' << 'README.md' <<
    '--line-numbers'

  spec.required_ruby_version = ">= 2.6.0"
  spec.files       = Dir["lib/**/*"]

  spec.add_dependency "em-websocket", "~> 0.5.0"
  spec.add_dependency "launchy", "~> 2.5.0"
end
