#------------------------------------------------------------------------------#
#  Copyleft 2022
#  This file is part of GuindillaGUI.
#  GuindillaGUI is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
#  GuindillaGUI is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with
#  GuindillaGUI. If not, see <https://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------#

module GuindillaGUI
  class Guindilla
  ##############################################################################
  #
  #                             Actions / Events
  #
  ##############################################################################
  ##
  # JavaScript `setTimeout` Timer
  def after(seconds, &block)
    Timer.new("timeout", seconds, &block)
  end

  def alert(message)
    send_js(%Q~ alert("#{message}") ~)
  end

  def append(element)
    id = caller_id(self)
    send_js(%Q~ $("##{self.id}").append($("##{element.id}")); ~)
  end

  ##
  # JavaScript `setInterval` Timer
  def every(seconds, &block)
    Timer.new("interval", seconds, &block)
  end

  def hide
    id = caller_id(self)
    send_js(%Q~ $("##{id}").hide(); ~)
  end

  ##
  # set position by x and y pixel coordinates
  def move_to(x, y)
    self.style(position: 'absolute') unless self.attributes.has_key?(:position)
    self.style(left: "#{x}px", top: "#{y}px")
  end

  def on_click(&block)
    id = caller_id(self)
    send_js(%Q~
      #{id}.onclick = function(event){
        socket.send("#{id}_click:!!:");
        event.stopPropagation();
      };
    ~)
    @@gui.blocks[:"#{id}_click"] = block if block_given?
    return @@gui.elements[:"#{id}"]
  end

  def on_hover(&block)
    id = caller_id(self)
    send_js(%Q~
      #{id}.onmouseover = function(event){
        socket.send("#{id}_hover:!!:");
        event.stopPropagation();
      };
    ~)
    @@gui.blocks[:"#{id}_hover"] = block if block_given?
    return @@gui.elements[:"#{id}"]
  end

  def on_leave(&block)
    id = caller_id(self)
    send_js(%Q~
      #{id}.onmouseout = function(event){
        socket.send("#{id}_leave:!!:");
        event.stopPropagation();
      };
    ~)
    @@gui.blocks[:"#{id}_leave"] = block if block_given?
    return @@gui.elements[:"#{id}"]
  end

  def on_mouse_move(&block)
    id = caller_id(self)
    send_js(%Q~
      #{id}.onmousemove = function(event){
        socket.send("#{id}:!!:mousemove:!!:" + event.pageX + ',' + event.pageY);
        event.stopPropagation();
      };
    ~)
    @@gui.blocks[:"#{id}_move"] = block if block_given?
    return @@gui.elements[:"#{id}"]
  end

  def rotate(degrees)
    rotation = self.attributes[:rotate].to_i + degrees
    self.style(rotate: "#{rotation}deg")
    return self
  end

  def show
    id = caller_id(self)
    send_js(%Q~ $("##{id}").show(); ~)
  end

  ##
  # show/hide element
  def toggle
    id = caller_id(self)
    send_js(%Q~ $("##{id}").toggle(); ~)
  end

  ##
  # `transition(property, duration, delay=0, type="ease")`
  # https://www.w3schools.com/css/css3_transitions.asp
  def transition(property, duration, *args)
    self.style(transition: "#{property} #{duration}s")
    unless args.empty?
      args.each do |arg|
        if arg.is_a?(String)
          self.style(transition_timing_function: "#{arg}")
        elsif arg.is_a?(Integer)
          self.style(transition_delay: "#{arg}s")
        end
      end
    end
  end
  alias_method :transition=, :transition


  ##############################################################################
  #
  #                               Elements
  #
  ##############################################################################
  ##
  # attributes may include `controls: true`
  def audio_out(attributes={})
    AudioVideo.new("audio", attributes)
  end

  def bullet_list(list_array, attributes={})
    ul = Element.new("ul")
    list_array.each do |li|
      send_js(%Q~ $("##{ul.id}").append($("<li>").html("#{li}")); ~)
    end
    return ul
  end

  ##
  # `button(text="", attributes={}, &block)`
  def button(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    txt = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    btn = Element.new("button", attributes)
    send_js(%Q~ #{btn.id}.innerHTML = "#{txt}"; ~)
    send_js(%Q~
      #{btn.id}.onclick = function(event){
        socket.send("#{btn.id}:!!:");
        event.stopPropagation();
      };
    ~)
    @@gui.blocks[:"#{btn.id}"] = block if block_given?
    return btn
  end

  def canvas(attributes={}, &block)
    attributes[:width] = "50%" unless attributes[:width]
    attributes[:height] = "33%" unless attributes[:height]
    Canvas.new(attributes, &block)
  end

  ##
  # `chart(type: 'scatter', title: "", attributes={}, &block)`
  def chart(attributes={}, &block)
    # OPTIMIZE: use Hash#merge!
    Chart.new(attributes, &block)
  end

  ##
  # `checkbox(label="", attributes={}, &block |boolean|)`
  def checkbox(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("checkbox", label, attributes, &block)
  end

  def circle(radius, attributes={})
    attributes[:cx] = radius
    attributes[:cy] = radius
    attributes[:r] = radius
    Svg.new("circle", attributes)
  end

  ##
  # `color_input(label="", attributes={}, &block |color|)`
  def color_input(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("color", label, attributes, &block)
  end

  def container(attributes={}, &block)
    Box.new(nil, attributes, &block)
  end

  ##
  # `file_input(label="", attributes={}, &block |file|)`
  def file_input(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("file", label, attributes, &block)
  end

  def h_box(attributes={}, &block)
    Box.new("row", attributes, &block)
  end

  def h_rule(attributes={})
    attributes[:width] = "100%" unless attributes[:width]
    Element.new("hr", attributes)
  end

  ##
  # `heading(level=1, string, attributes={})`
  def heading(*args)
    args.unshift(1) unless args[0].is_a?(Integer)
    level = args[0]
    string = args[1]
    args[2] ? attributes = args[2] : attributes = {}
    h = Element.new("#{'h' + level.to_s}", attributes)
    send_js(%Q~ #{h.id}.innerHTML = "#{string}"; ~)
    return h
  end

  def image(source, attributes={})
    img = Element.new("img", attributes)
    send_js(%Q~ #{img.id}.src = "#{source}"; ~)
    send_js(%Q~ #{img.id}.onerror = function(){
        #{img.id}.src = "resources/guindilla_gui.png"
      };
    ~)
    return img
  end

  def line_break
    para("")
  end

  def link(url, attributes={})
    lnk = Element.new("a", attributes)
    if self.is_a?(GuindillaGUI::Element)
      lnk.append(self)
    else
      attributes[:text] ? txt = attributes[:text] : txt = url
      send_js(%Q~ #{lnk.id}.textContent = "#{txt}"; ~)
    end
    send_js(%Q~ #{lnk.id}.href = "#{url}"; ~)
    send_js(%Q~ #{lnk.id}.target = "_blank"; ~)
    return lnk
  end

  ##
  # `number_input(label="", attributes={}, &block |input|)`
  def number_input(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("number", label, attributes, &block)
  end

  def ordered_list(list_array, attributes={})
    ol = Element.new("ol")
    list_array.each do |li|
      send_js(%Q~ $("##{ol.id}").append($("<li>").html("#{li}")); ~)
    end
    return ol
  end

  def para(string, attributes={})
    p = Element.new("p", attributes)
    send_js(%Q~ #{p.id}.innerHTML = "#{string}"; ~)
    return p
  end

  ##
  # `polygon` takes a nested array of x, y coordinates, e.g.:
  # `polygon([[2, 0], [0, 3], [3, 3], [2, 0]])`
  def polygon(points_array, attributes={})
    points = ""
    points_array.each do |arr|
      points += arr.join(",")
      points += " "
    end
    attributes[:points] = points
    width = 0
    height = 0
    points.split(" ").each do |xy|
      pair = xy.split(",")
      width = pair[0].to_i if pair[0].to_i > width
      height = pair[1].to_i if pair[1].to_i > height
    end
    attributes[:width] = width unless attributes[:width]
    attributes[:height] = height unless attributes[:height]
    Svg.new("polygon", attributes)
  end

  ##
  # `radio_button(label="", attributes={}, &block |boolean|)`
  # attributes may include a `group:`` key to group buttons
  def radio_button(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    attributes[:group] = "no_group" unless attributes[:group]
    Input.new("radio", label, attributes, &block)
  end

  ##
  # `range_slider(label="", {min: 0, max: 1.0, step: 0.05, value: 0.5}, &block |value|)`
  def range_slider(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    attributes[:min] = 0 unless attributes[:min]
    attributes[:max] = 1.0 unless attributes[:max]
    attributes[:step] = 0.05 unless attributes[:step]
    attributes[:value] = (attributes[:max] - attributes[:min]) / 2 unless attributes[:value]
    Input.new("range", label, attributes, &block)
  end

  def rectangle(width_integer, height_integer, attributes={})
    attributes[:width] = width_integer
    attributes[:height] = height_integer
    Svg.new("rect", attributes)
  end

  def source(path_string)
    id = caller_id(self)
    send_js(%Q~ #{id}.src = "#{path_string}"; ~)
  end
  alias_method :source=, :source

  def square(size_integer, attributes={})
    rectangle(size_integer, size_integer, attributes)
  end

  def sub_script(string, attributes={})
    ## FIXME: displays superscript, huh???
    sub = Element.new("p", attributes)
    send_js(%Q~ #{sub.id}.innerHTML = "#{string}"; ~)
    sub.style(vertical_align: "sub", font_size: "small")
    return sub
  end

  def sup_script(string, attributes={})
    sup = Element.new("p", attributes)
    send_js(%Q~ #{sup.id}.innerHTML = "#{string}"; ~)
    sup.style(vertical_align: "super", font_size: "small")
    return sup
  end

  ##
  # set element text
  def text(string)
    id = caller_id(self)
    send_js(%Q~ #{id}.innerHTML = "#{string}"; ~)
  end
  alias_method :text=, :text

  ##
  # `text_input(label="", attributes={}, &block |input|)`
  def text_input(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("text", label, attributes, &block)
  end

  def triangle(size_integer, attributes={})
    polygon(
      [
        [0, size_integer * 0.866],
        [size_integer / 2, 0],
        [size_integer, size_integer * 0.866]
      ],
      attributes
    )
  end

  ##
  # `url_input(label="", attributes={}, &block |input|)`
  def url_input(*args, &block)
    args.unshift("") unless args[0].is_a?(String)
    label = args[0]
    args[1] ? attributes = args[1] : attributes = {}
    Input.new("url", label, attributes, &block)
  end

  def v_box(attributes={}, &block)
    Box.new("column", attributes, &block)
  end

  ##
  # attributes may include `controls: true`
  def video_out(attributes={})
    attributes[:width] = 500 unless attributes[:width]
    attributes[:height] = 300 unless attributes[:height]
    AudioVideo.new("video", attributes)
  end

  def web_frame(attributes={})
    attributes[:height] = "500px" unless attributes[:height]
    Element.new("iframe", attributes)
  end


  ##############################################################################
  #
  #                               Styles
  #
  ##############################################################################
  ##
  # CSS properties key/value pairs with underscores `_` rather than dashes `-`
  # e.g. `style(color: 'blue', text_align: 'center')`
  def style(hash)
    id = caller_id(self)
    hash.each do |key, value|
    send_js(%Q~
      $("##{id}").css("#{key.to_s.gsub("_","-")}", "#{value}");
    ~)
    @@gui.elements[:"#{id}"].attributes[:"#{key}"] = "#{value}"
    end
    return self
  end
  alias_method :style=, :style

  ##
  # Vertical alignment within a box.
  # Position may be `'stretch'`(default), `'center'`, `'start'`, `'end'`, or `'baseline'`.
  # See 'align-items': https://css-tricks.com/snippets/css/a-guide-to-flexbox/
  def align(position)  # vertical
    position = "flex-start" if position == "start"
    position = "flex-end" if position == "end"
    self.style(align_items: "#{position}")
    return self
  end
  alias_method :align=, :align

  ##
  # `color_string` may be rgb, rgba, hex, or CSS color
  def background(color_string)
    self.style(background: "#{color_string}")
    return self
  end
  alias_method :background=, :background

  ##
  # https://www.w3schools.com/css/css_border.asp
  # e.g. `border('2px solid blue')`
  def border(border_string)
    self.style(border: "#{border_string}")
    return self
  end
  alias_method :border=, :border

  ##
  # `color_string` may be rgb, rgba, hex, or CSS color
  def border_color(color_string)
    if self.class.to_s == "GuindillaGUI::Svg"
      self.style(stroke: "#{color_string}")
    else
      self.style(border_color: "#{color_string}")
    end
    return self
  end
  alias_method :border_color=, :border_color

  ##
  # https://www.w3schools.com/cssref/css3_pr_border-radius.asp
  def border_radius(pixels)
    self.style(border_radius: "#{pixels}")
    return self
    end
  alias_method :border_radius=, :border_radius

  def border_width(width)
    self.style(border_width: "#{width_integer}")
    return self

  end
  alias_method :border_width=, :border_width

  ##
  # `color_string` may be rgb, rgba, hex, or CSS color
  def color(color_string)
    if self.class.to_s == "GuindillaGUI::Svg"
      self.style(fill: "#{color_string}")
    else
      self.style(color: "#{color_string}")
    end
    return self
  end
  alias_method :color=, :color

  ##
  # CSS display property.
  # https://www.w3schools.com/CSSref/pr_class_display.asp
  def display(property)
    self.style(display: "#{property}")
    return self
  end
  alias_method :display=, :display

  ##
  # https://www.w3schools.com/css/css_font.asp
  def font(family_string, size_integer)
    id = caller_id(self)
    self.style(font_family: "#{family_string}", font_size: "#{size_integer}")
    return self
  end
  alias_method :font=, :font

  ##
  # https://www.w3schools.com/Css/css_font.asp
  def font_family(family_string)
    self.style(font_family: "#{family_string}")
    return self
  end
  alias_method :font_family=, :font_family

  ##
  # https://www.w3schools.com/cssref/pr_font_font-size.asp
  def font_size(size)
    self.style(font_size: "#{size}")
    return self
  end
  alias_method :font_size=, :font_size

  ##
  # https://www.w3schools.com/cssref/pr_dim_height.asp
  def height(h)
    self.style(height: "#{h}")
    return self
  end
  alias_method :height=, :height

  ##
  # Horizontal justification within a box.
  # https://www.w3schools.com/csSref/css3_pr_justify-content.asp
  def justify(position)
    position = "flex-start" if position == "start"
    position = "flex-end" if position == "end"
    self.style(justify_content: "#{position}")
    return self
  end
  alias_method :justify=, :justify

  ##
  # margin in pixels, or `'auto'` to center
  def margin(size)
    self.style(margin: "#{size}")
    return self
  end
  alias_method :margin=, :margin

  ##
  # 0.0 to 1.0
  def opacity(value)
    if self.class.to_s == "GuindillaGUI::Svg"
      self.style(fill_opacity: "#{value}")
    else
      self.style(opacity: "#{value}")
    end
    return self
  end
  alias_method :opacity=, :opacity

  ##
  # padding in pixels
  def padding(size)
    self.style(padding: "#{size}")
    return self
  end
  alias_method :padding=, :padding

  def size(width, height)
    self.style(width: "#{width}", height: "#{height}")
    return self
  end
  alias_method :size=, :size

  ##
  # position may be `'left'`, `'right'`, `'center'`, or `'justify'`
  def text_align(position)
    self.style(text_align: "#{position}")
    return self
  end
  alias_method :text_align=, :text_align

  ##
  # https://www.w3schools.com/cssref/pr_dim_width.asp
  def width(w)
    self.style(width: "#{w}")
    return self
  end
  alias_method :width=, :width

  end #class Guindilla
end #module
