#------------------------------------------------------------------------------#
#  Copyleft 2022
#  This file is part of GuindillaGUI.
#  GuindillaGUI is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
#  GuindillaGUI is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with
#  GuindillaGUI. If not, see <https://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------#

module GuindillaGUI
  ##
  # Normally `Guindilla` will be the only class you explicitly instantiate.
  # Other classes are instantiated by methods called inside the `Guindilla` block.
  # Option keys may include `verbose`, `host:` , and/or `port:`
  class Guindilla
    attr_accessor :socket, :active_id, :elements, :blocks, :inputs

    def initialize(options={}, &block)
      @@gui = self
      @active_id = 'body'
      @elements = {}
      @blocks = {}
      @inputs = {}
      html_file = File.expand_path File.dirname(__FILE__) + "/resources/guindilla.html"
      options[:host] ? host = options[:host] : host = 'localhost'
      options[:port] ? port = options[:port] : port = 8181

      # initialize socket client #
      Launchy.open(html_file) do |exception|
        puts "Attempted to open #{html_file} and failed because #{exception}"
      end

      # initialize socket server #
      EM.run do
        EM::WebSocket.start(:host => host, :port => port) do |socket|
          @socket = socket

          socket.onopen do |handshake|
            puts "GuindillaGUI WebSocket connection open on: #{host}, port #{port}"
            # make the main box #
            v_box do
              # pass off to user #
              self.instance_eval &block if block
            end
          end

          # handle events #
          socket.onmessage do |msg|
            puts "Recieved message: #{msg}" if options[:verbose] == true
            ##
            # not sure how necessary this is, but think it handles funky closes
            socket.close if msg == "UI closed." # msg from guindilla.js

            message = msg.split(":!!:")
            id = message[0]

            if message[1]
              case message[1]
              when "position" # REVIEW: not so nice
                element = @elements[:"#{id}"]
                message[2].chop!.split(",").each do |pair|
                  keyval = pair.split(":")
                  element.position[:"#{keyval[0]}"] = keyval[1].to_f
                end
                @blocks[:"#{id}_pos"].call(element.position) if @blocks[:"#{id}_pos"]
              when "input"
                message[2] = "" unless message[2]
                begin
                  message[2] = Integer(message[2])
                rescue ArgumentError => e
                end
                @inputs[:"#{id}"].value = message[2]
                @blocks[:"#{id}"].call(message[2]) if @blocks[:"#{id}"]
              when "mousemove"
                xy = message[2].split(',')
                @blocks[:"#{id}_move"].call(xy[0].to_i, xy[1].to_i) if @blocks[:"#{id}_move"]
              end
            else
              @blocks[:"#{id}"].call if @blocks[:"#{id}"]
            end
          end #socket.onmessage

          socket.onclose do
            #send_js(%Q~ window.stop(); process.exit(1); ~) # maybe not necessary
            puts "GuindillaGUI WebSocket connection closed"
            #abort "exiting GuindillaGUI..."
            puts "exiting GuindillaGUI..."
            exit!
          end

        end #EM::WebSocket.run
      end #EM.run
    end #initialize

    def attributes  # REVIEW: not sure this is needed (it's only for stand-alone) #
      @@gui.elements[:"#{caller_id(self)}"].attributes
    end

    private
    ##
    # `caller_id(caller)` allows for stand-alone style methods
    #  from within an element's block, eg:
    # ```
    # h_box do
    #   background('yellow')
    # end
    # ```
    def caller_id(caller)
      if caller.class.to_s == "GuindillaGUI::Guindilla"
        id = @@gui.active_id
      else
        id = caller.id
      end
    end

    def send_js(script)
      @@gui.socket.send(%Q~ <script>#{script}</script> ~)
    end
  end #class Guindilla


  ##############################################################################
  #
  #                              Element
  #
  ##############################################################################
  ##
  # `Element` parent class that Guindilla methods use to create HTML elements.
  class Element < Guindilla
    attr_reader :id
    attr_accessor :attributes

    def initialize(type, attributes={})
      @id = "#{type}_#{Time.now.hash.to_s.gsub('-', 'x')}"
      send_js(%Q~
        const #{@id} = document.createElement("#{type}");
        #{@id}.id = "#{@id}";
      ~)

      hidden = attributes.delete(:hidden)
      self.hide if hidden == true
      send_js(%Q~ #{@@gui.active_id}.append(#{@id}); ~)

      if attributes.has_key?(:size)
        size = attributes[:size].split(",")
        attributes[:width] = size[0].to_i
        attributes[:height] = size[1].to_i
      end

      @attributes = attributes
      @@gui.elements[:"#{@id}"] = self
      self.style(attributes)
    end

    ##
    # REVIEW: this is ugly
    def get_position(&block)
      @@gui.blocks[:"#{self.id}_pos"] = block if block_given?
      send_js(%Q~
        var rect = #{@id}.getBoundingClientRect();
        var rect_string = ""
        for (var key in rect) {
          if(typeof rect[key] !== 'function') {
            rect_string += `${key}:${rect[key]},`;
          }
        }
        socket.send("#{id}:!!:position:!!:" + rect_string)
      ~)
    end
  end #class Element


  ##############################################################################
  #
  #                             AudioVideo
  #
  ##############################################################################
  class AudioVideo < Element
    attr_accessor :state

    def initialize(type, attributes)
      super("#{type}")
      @state = "stopped"
      if attributes[:controls] == true
        send_js(%Q~ #{self.id}.controls = true; ~)
      end
      self.width = attributes[:width] if attributes[:width]
      self.height = attributes[:height] if attributes[:height]
      self.source = attributes[:source] if attributes[:source]
    end

    def pause
      send_js(%Q~ #{self.id}.pause(); ~)
      @state = "paused"
    end

    def play
      send_js(%Q~ #{self.id}.play(); ~)
      @state = "playing"
    end

    def volume=(vol)
      send_js(%Q~ #{self.id}.volume = #{vol}; ~)
    end
  end #class AudioVideo


  ##############################################################################
  #
  #                                Box
  #
  ##############################################################################
  class Box < Element
    attr_reader :direction

    def initialize(direction, attributes, &block)
      attributes[:justify_content] = attributes[:justify] if attributes[:justify]
      attributes[:align_items] = attributes[:align] if attributes [:align]
      super("div", attributes)

      unless direction == nil
        send_js(%Q~
          #{self.id}.style.display = 'flex';
          #{self.id}.style.flexFlow = '#{direction} wrap';
        ~)
      end

      last_active_id = @@gui.active_id
      @@gui.active_id = self.id
      if block_given?
        block.call
      end
      @@gui.active_id = last_active_id
    end #initialize
  end #class Box


  ##############################################################################
  #
  #                              Canvas
  #
  ##############################################################################
  class Canvas < Element
    def initialize(attributes, &block)
      super("canvas", attributes)
      send_js(%Q~ const #{self.id}_ctx = #{self.id}.getContext("2d"); ~)
      self.instance_eval &block if block_given?
    end

    ##
    # attributes may include `type: fill`, and/or `reverse: true`
    def arc(x, y, radius, start_angle, end_angle, attributes={})
      attributes[:type] ? type = attributes[:type] : type = 'stroke'
      attributes[:reverse] ? reverse = attributes[:reverse] : reverse = false
      s_ang = (start_angle) * Math::PI / 180
      e_ang = (end_angle) * Math::PI / 180
      send_js(%Q~
        #{self.id}_ctx.arc(#{x}, #{y}, #{radius}, #{s_ang}, #{e_ang}, #{reverse});
        #{self.id}_ctx.#{type.to_s}();
      ~)
    end

    def arc_to(x1, y1, x2, y2, radius) ## HACK: works, but wtf is this? ##
      send_js(%Q~
        #{self.id}_ctx.arc(#{x1}, #{y1}, #{x2}, #{y2}, #{radius});
        #{self.id}_ctx.stroke();
      ~)
    end

    def canvas_circle(x, y, radius, type="stroke")
      arc(x, y, radius, 0, 360, type: "#{type}")
      send_js(%Q~ #{self.id}_ctx.fill; ~) if type.to_s == "fill"
    end

    def canvas_rectangle(x, y, width, height, type="stroke")
      send_js(%Q~
        #{self.id}_ctx.#{type.to_s}Rect(#{x}, #{y}, #{width}, #{height});
      ~)
    end

    def draw(type="stroke", &block)
      send_js(%Q~ #{self.id}_ctx.beginPath(); ~)
      block.call
      send_js(%Q~ #{self.id}_ctx.#{type.to_s}(); ~)
    end

    def fill
      send_js(%Q~ #{self.id}_ctx.fill(); ~)
    end

    def fill_color(clr)
      send_js(%Q~ #{self.id}_ctx.fillStyle = "#{clr}"; ~)
    end

    def line_color(clr)
      send_js(%Q~
        #{self.id}_ctx.strokeStyle = "#{clr}";
      ~)
    end

    def line_to(x, y)
      send_js(%Q~
        #{self.id}_ctx.lineTo(#{x}, #{y});
        #{self.id}_ctx.stroke();
      ~)
    end

    def move_to(x, y)
      send_js(%Q~
        #{self.id}_ctx.moveTo(#{x}, #{y});
      ~)
    end

    def stroke
      send_js(%Q~ #{self.id}_ctx.stroke(); ~)
    end
  end #class Canvas


  ##############################################################################
  #
  #                               Chart
  #
  ##############################################################################
  class Chart < Element
    attr_accessor :data

    def initialize(attributes, &block)
      title = attributes.delete(:title)
      attributes[:type] = 'scatter' unless attributes.has_key?(:type)
      @type = attributes.delete(:type)
      attributes[:showlegend] = true unless attributes.has_key?(:showlegend)
      showlegend = attributes.delete(:showlegend)
      div = container(attributes)
      @data = []
      @x_axis = {}
      @y_axis = {}
      @legend = {}

      instance_eval &block if block

      data_string = "["
      @data.each{|hash| data_string += to_plotly(hash) + ", "}
      data_string.delete_suffix!(", ")
      data_string += "]"

      layout_string = %Q~{title: "#{title}", showlegend: #{showlegend}, ~
      layout_string += "xaxis: " + to_plotly(@x_axis) + ", " unless @x_axis.empty?
      layout_string += "yaxis: " + to_plotly(@y_axis) + ", " unless @y_axis.empty?
      layout_string += "legend: " + to_plotly(@legend) + ", " unless @legend.empty?
      layout_string.delete_suffix!(", ")
      layout_string += "}"

      send_js(%Q~ Plotly.newPlot(#{div.id}, #{data_string}, #{layout_string}); ~)
    end #initialize

    def plot(plot_hash)
      plot_hash[:type] = @type
      @data << plot_hash
    end

    def legend(legend_hash)
      @legend = legend_hash
    end

    def x_axis(x_hash)
      @x_axis = x_hash
    end

    def y_axis(y_hash)
      @y_axis = y_hash
    end

    private

    def to_plotly(hash)
      string = "{"
      hash.each do |key, value|
        if value.is_a?(String)
          string += %Q~#{key}: "#{value}", ~
        elsif value.is_a?(Hash)
          string += "#{key}: "
          string += to_plotly(value)
          string.delete_suffix!(", ")
          string += ", "
        else
          string += "#{key}: #{value}, "
        end
      end
      string.delete_suffix!(", ")
      string += "}"
      return string
    end
  end # class Chart


  ##############################################################################
  #
  #                                 Input
  #
  ##############################################################################
  class Input < Element
    attr_accessor :label, :value

    def initialize(type, label, attributes, &block)
      attributes[:hidden] = true
      min = attributes.delete(:min) if attributes.has_key?(:min)
      max = attributes.delete(:max) if attributes.has_key?(:max)
      step = attributes.delete(:step) if attributes.has_key?(:step)
      value = attributes.delete(:value) if attributes.has_key?(:value)
      super("input", attributes)

      send_js(%Q~ #{self.id}.type = "#{type}"; ~)
      @@gui.inputs[:"#{self.id}"] = self
      @@gui.blocks[:"#{self.id}"] = block if block_given?
      group = attributes[:group]   #####

      case type
      when "checkbox"
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.checked);
            event.stopPropagation();
          };
        ~)
        self.check if attributes[:checked]
      when "color"
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.value);
            event.stopPropagation();
          };
        ~)
      when "file"
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + URL.createObjectURL(#{self.id}.files[0]));
            event.stopPropagation();
          };
          #{self.id}.setAttribute('multiple', '');
        ~)
      when "number"
        send_js(%Q~
          #{self.id}.setAttribute('min', '#{min}') ;
          #{self.id}.setAttribute('max', '#{max}') ;
        ~)
        send_js(%Q~
          #{self.id}.onchange = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.value);
            event.stopPropagation();
          };
        ~)
      when "radio"
        send_js(%Q~ #{self.id}.name = "#{group}"; ~)
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.checked);
            event.stopPropagation();
          };
        ~)
        self.check if attributes[:checked]
      when "range"
        send_js(%Q~
          #{self.id}.setAttribute('min', '#{min}') ;
          #{self.id}.setAttribute('max', '#{max}') ;
          #{self.id}.setAttribute('step', '#{step}') ;
          #{self.id}.setAttribute('value', '#{value}') ;
        ~)
        send_js(%Q~
          #{self.id}.onchange = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.value);
            event.stopPropagation();
          };
        ~)
      when "text"
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.value);
            event.stopPropagation();
          };
        ~)
      when "url"
        send_js(%Q~
          #{self.id}.oninput = function(event){
            socket.send("#{self.id}:!!:input:!!:" + #{self.id}.value);
            event.stopPropagation();
          };
        ~)
      end #case type

      @label = Element.new("label")
      send_js(%Q~ #{@label.id}.innerHTML = "#{label}"; ~)
      @label.append(self)
      @label.show
      return self  ## ?
    end #initialize

    def check
      send_js(%Q~ #{self.id}.checked = true; ~)
    end

    def uncheck
      send_js(%Q~ #{self.id}.checked = false; ~)
    end
  end #class Input


  ##############################################################################
  #
  #                                Svg
  #
  ##############################################################################
  class Svg < Element
    attr_reader :type

    def initialize(type, attributes, &block)
      attributes[:fill] = attributes.delete(:color) if attributes.has_key?(:color)
      if attributes.has_key?(:r)
        attributes[:width] = attributes[:r] * 2
        attributes[:height] = attributes[:r] * 2
      end
      @type = type
      hidden = attributes.delete(:hidden)
      @id = "svg_#{Time.now.hash.to_s.gsub('-', 'x')}"
      svg_ns = "http://www.w3.org/2000/svg"

      send_js(%Q~
        const #{@id} = document.createElementNS("#{svg_ns}", "svg");
        #{@id}.id = "#{@id}";
        #{@id}.setAttribute("width", #{attributes[:width]});
        #{@id}.setAttribute("height", #{attributes[:height]});
        const #{type}_#{@id} = document.createElementNS("#{svg_ns}", "#{type}");
        #{type}_#{@id}.id = "#{type}_#{@id}";
      ~)

      attributes.each do |key, value|
        send_js(%Q~
          #{type}_#{@id}.setAttribute("#{key}", "#{value}");
        ~)
      end

      send_js(%Q~ #{self.id}.appendChild(#{type}_#{@id}); ~)
      send_js(%Q~ #{@@gui.active_id}.append(#{@id}); ~)
      self.hide if hidden == true

      @attributes = attributes
      @@gui.elements[:"#{@id}"] = self
      return self  ###
    end #initialize
  end #class Svg


  ##############################################################################
  #
  #                              Timer
  #
  ##############################################################################
  class Timer < Guindilla
    attr_reader :id, :running, :secs

    def initialize(type, seconds, &block)
      @id = "timer_#{Time.now.hash.to_s.gsub('-', 'x')}"
      @secs = seconds
      @@gui.blocks[:"#{@id}"] = block if block_given?

      if type == "interval"
        send_js(%Q~
          let #{@id} = setInterval(function(){
            socket.send("#{@id}:!!:");
          }, #{@secs * 1000});
        ~)
      elsif type == "timeout"
        send_js(%Q~
          let #{@id} = setTimeout(function(){
            socket.send("#{@id}:!!:");
          }, #{@secs * 1000});
        ~)
      end
      @running = true
    end

    def start
      send_js(%Q~
        #{@id} = setInterval(function(){
          socket.send("#{@id}:!!:");
        }, #{@secs * 1000});
      ~)
      @running = true
    end

    def stop
      send_js(%Q~ clearInterval(#{self.id}) ~)
      @running = false
    end

    def toggle
      self.running ? self.stop : self.start
    end
  end #class Timer

end #module GuindillaGUI
