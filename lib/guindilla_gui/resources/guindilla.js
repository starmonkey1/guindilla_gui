/*
#------------------------------------------------------------------------------#
#  Copyleft 2022
#  This file is part of GuindillaGUI.
#  GuindillaGUI is free software: you can redistribute it and/or modify it under
#  the terms of the GNU General Public License as published by the Free Software
#  Foundation, either version 3 of the License, or (at your option) any later
#  version.
#  GuindillaGUI is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
#  FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License along with
#  GuindillaGUI. If not, see <https://www.gnu.org/licenses/>.
#------------------------------------------------------------------------------#
*/

let socket = null

$(document).ready(function(){

  socket = new WebSocket('ws://localhost:8181/');

  socket.onopen = function(event) {
    console.log("[open] Connection with GuindillaGUI established");
  };
  socket.onmessage = function(event) {
    console.log(`[message] Data received from server: ${event.data}`);
    $(document.body).append(event.data);
  };
  socket.onclose = function(event) {
    if (event.wasClean) {
      console.log(`[close] Connection with GuindillaGUI closed, code=${event.code} reason=${event.reason}`);
    } else {
      alert('[close] Connection with GuindillaGUI died');
    }
  };
  socket.onerror = function(error) {
    alert(`[error] ${error.message}`);
  };

  window.addEventListener('beforeunload', function (e) {
    socket.send("UI closed.")
 	  socket.close();
  });

});
